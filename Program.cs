using System;

namespace ejercicios_array_master
{
    class Program
    {
        static void Main(string[] args)
        {
            
            /*
             Realizar los siguientes ejercicios y subirlos a un repositorio con el nombre "ejercicios-array".
1.	Un programa que pida al usuario 4 números, los memorice (utilizando un array), calcule su media aritmética y después muestre en pantalla la media y los datos tecleados.
int[] numeros = {27, 8, 20, 17}; 
for (int i = 0; i < numeros.Length; i++) { Console.WriteLine(numeros[i]); }

2.	Un programa que pida al usuario 5 números reales (pista: necesitarás un array de "float") y luego los muestre en el orden contrario al que se introdujeron.
float[] Algo = new float[5]{1,5,13,11,20}; 
Algo[4] = 1; Algo[3] = 5; Algo[2] = 13; Algo[1] = 11;
 Algo[0] = 20; for (int i = 0; i < Algo.Length; i++) { Console.WriteLine(Algo[i]); }
3.	Un programa que almacene en un array el número de días que tiene cada mes (supondremos que es un año no bisiesto), pida al usuario que le indique un mes (1=enero, 12=diciembre) y muestre en pantalla el número de días que tiene ese mes.
Console.WriteLine("Ingrese un numero del 1 al 12");

   int[] dias = new int[4];
   dias[1] = 31;
   dias[2] = 30;
   dias[3] = 28;

   int[] meses = new int[13];
   meses[1] = dias[1];
   meses[2] = dias[3];
   meses[3] = dias[1];
   meses[4] = dias[2];
   meses[5] = dias[1];
   meses[6] = dias[2];
   meses[7] = dias[1];
   meses[8] = dias[1];
   meses[9] = dias[2];
   meses[10] = dias[1];
   meses[11] = dias[2];
   meses[12] = dias[1];

   int mes = 0;
   mes = Convert.ToInt32(Console.ReadLine());
   Console.WriteLine("El mes número {0} tiene {1} dias", mes, meses[mes]);

4.	Un programa que pida al usuario 10 números y luego calcule y muestre cuál es el mayor de todos ellos.
int[] valores = new int[10]; 
valores[0] = 43;
 valores[1] = 17;
 valores[2] = 55;
 valores[3] = 20; 
valores[4] = 80;
 valores[5] = 32;
 valores[6] = 7;
 valores[7] = 2;
 valores[8] = 9; 
valores[9] = 72;
 Console.WriteLine(valores.Max());



5.	Un programa que prepare espacio para un máximo de 100 nombres. El usuario deberá ir introduciendo un nombre cada vez, hasta que se pulse Intro sin teclear nada, momento en el que dejarán de pedirse más nombres y se mostrará en pantalla la lista de los nombres que se han introducido.
string[] nombres = new string[100];
 for (int i = 0; i < nombres.Length; i++) { 
Console.WriteLine("Ingrese el nombre de la persona "+ (i+1)); 
nombres[i] = Console.ReadLine(); 
} Console.Clear();


            */
        }
    }
}
